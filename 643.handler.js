const parseUtils = require('./parseUtils.js');
const axios = require('axios');
const typeConverters = require('./messageToHex')
const moment  = require('moment');
let devConfig; //keep this here plz. 

//let timeValue = 'S2=7F2E16'
const configMessage =',S0=98,S11=TEK643,S12=c2.korem2m.com,S15=monitors.angusenergy.net,S16=8080,S4=0,S5=0,S6=0,S26=20';
const closeMessage = ',R1=80';
const askForConfig = 'TEK643,R1=02'
const askForReading = 'TEK643,R1=20'
const devUrl ='https://qa.angusdev.com/tms';

let url = process.env.TMSURL || devUrl
let dataCount = 0;
let timeHashMap = {};
let socketHashMap = {};

function getTimeSetting(hour, min){

    const days = 1111111;
    const daySchedule = parseUtils.setBit(7, 7, 0)

    let timeSchedule = parseUtils.setBit(0, 7, 0);

    timeSchedule |= hour << 2;
    timeSchedule |= min /15;

    let hexTime = (timeSchedule>>>0).toString(16)

    if(hexTime.length === 1){
      hexTime = '0' + hexTime
    }

    const fullTimeString = `${(127>>>0).toString(16)}${hexTime}16`

    return `,S2=${fullTimeString.toUpperCase()}`
}

function get_set_Config(socket, settingsObj, timeSetting, trace) {

    let activeMessage = `TEK643,R3=ACTIVE,R2=${moment().format('YY/MM/DD:HH/mm/ss')}`;
    let time = '';
    let config = configMessage;

    if(timeSetting){

      time = getTimeSetting(
        timeSetting.hour, 
        timeSetting.min
      );

      console.log(`get_set_config`, `SERIAL=${settingsObj.serial}`, `SETTING TIME TO =${time}`);

    } else {
    	activeMessage += ',R1=80';
      console.log(`get_set_config`, `SERIAL=${settingsObj.serial}`, `NOT SETTING TIME! TIME ALREADY SET.`);
    }

    if (settingsObj.serial == "861075026510412") {
    	console.log("TESTING MONITOR!!!!!");
    	config =',S0=98,S11=TEK643,S12=c2.korem2m.com,S15=monitors.angusenergy.net,S16=8080,S4=0,S5=0,S6=0,S26=20';
    }

    let message = `${activeMessage}${time}${config}`;

    console.log(`get_set_config`, `SERIAL=${settingsObj.serial}`, `MESSAGE=${message}`);
    console.log(`get_set_config`, `SERIAL=${settingsObj.serial}`, `HEX    =${typeConverters.messageToHex(message)}`)

    try {
      socket.attachment = socket.attachment || {};
      socket.attachment["MESSAGE"] = message;
      socket.attachment["MESSAGE-SCHEDULE"] = time;
      socket.attachment["MESSAGE-HEX"] = typeConverters.messageToHex(message);

      socket.write(message) 
    } catch(err) {
        alert({
          "SERIAL": settingsObj.serial,
          "ERROR": err.message || err,
          "MESSAGE": message,
          "STATE": settingsObj,
          "HEX-MESSAGE": typeConverters.messageToHex(message),
          "TRACE": trace,
          "SCHEDULE": time
        }, "SOCKET.WRITE")
    }

    
     // socket.write(askForConfig)
}

function alert(packet, location) {
  console.log(`SENT ALERT | ${location} | ${JSON.stringify(packet, null, 2)}`);
  // axios.post("http://172.16.150.42:6070/event", {
  //   key: "MONITOR",
  //   tag: "643.ENDPOINT",
  //   origin: location,
  //   detail: JSON.stringify(packet, null, 1),
  //   level: "ERROR"
  // }).catch(err => {
  //     console.log(`THERE WAS AN WITH ALERT`);
  //     console.log(err);     
  //   })
}

function endConnection(active,settingsObj, {socket, dataCount}, time, trace) {
    let sendURL= `https://support.paygo.net`;
    let shouldEnd = active && dataCount >= 3;

    if(shouldEnd){   
      const message = `TEK643${closeMessage},R3=ACTIVE,R2=${moment().format('YY/MM/DD:HH/mm/ss')}`;

      dataCount = 0;

      console.log(`ENDING CONN FOR ${settingsObj.serial} | MESSAGE = ${message}`);

      try {
        socket.attachment = socket.attachment || {};
        socket.attachment["MESSAGE"] = message;
        socket.attachment["MESSAGE-SCHEDULE"] = time;
        socket.attachment["MESSAGE-HEX"] = typeConverters.messageToHex(message);

        socket.end(message);
      } catch(err) {
          alert({
            "SERIAL": settingsObj.serial,
            "ERROR": err.message || err,
            "MESSAGE": message,
            "STATE": settingsObj,
            "HEX-MESSAGE": typeConverters.messageToHex(message),
            "TRACE": trace,
            "SCHEDULE": time
          }, "SOCKET.END")
      }

      delete socketHashMap[settingsObj.serial];

    } else{
      console.log(`SETTING CONFIG ${settingsObj.serial} | TIME ?= ${JSON.stringify(time || {})}`);

      get_set_Config(socket, settingsObj, time, trace)
    }

    console.log("THIS >", settingsObj)

    axios.post(
      `https://support.paygo.net/tms/set643settings`, 
      settingsObj 
    )
    .then(res => console.log(`ENDING CONN | set643settings FOR ${settingsObj.serial}`, res.data))
    .catch(err => {
        alert({
          "SERIAL": settingsObj.serial,
          "ERROR": err.message || err,
          "PAYLOAD": settingsObj,
          "URL": "https://support.paygo.net/tms/set643settings",
          "TRACE": trace,
        }, "FEEDBACK.LOOP STATE RECORD")
    })
}



//This is to randomise the call in times of the monitors. TODO!!! these are randomizing 


/*
  for all bit operations please checkout the TCPSerialiser in the c# code provited by tekelek
*/

const TEK643 = (data, socket) => {
  const RAW_DATA = data;
  return new Promise((resolve, reject)=>{

    //The payloadId used for most of the actual parsing out. Its a simple buffer in hexidecible format
    const payLoadId = new Buffer(data, 'hex')
    const messageStartIndex = 17;
    const payLoad = data.toString('hex')
    console.log(payLoad,'incoming')
    let PayloadMessage = [];
    let stateMessage = {};
    let time = 0;
    let monitorActive;
    for ( i = 9; i < (payLoad.length - messageStartIndex); i += 4){
        const data = {};
        const JSONData = data.JSONData = {};
        JSONData.SERIAL = parseUtils.getMonitorId(payLoadId);
        stateMessage.serial = JSONData.SERIAL;
        JSONData.NAME = 'TEK643 - Propane';
        JSONData.KEY = 2;
        JSONData.PRID = payLoadId[0];
        const gsmRssi = payLoadId[5];
        monitorActive = parseUtils.getByteBit(payLoadId[4], 7)
        stateMessage.isActive = monitorActive
        JSONData.gsmRssi = gsmRssi;
        stateMessage.rssi = gsmRssi
        JSONData.active = monitorActive;
        JSONData.messageCount = (payLoadId[0 + messageStartIndex] << 8) + payLoadId[1 + messageStartIndex];
        JSONData.retryTickets = payLoadId[2 + messageStartIndex] >> 5;
        JSONData.rtcHours = payLoadId[2 + messageStartIndex] & 31;
        JSONData.DIAG = (payLoadId[3] << 8) + payLoadId[4];
        JSONData.configVersion = payLoadId[5];
        JSONData.loggerSpeed = payLoadId[6 + messageStartIndex];
        JSONData.rtcMinutes = payLoadId[8 + messageStartIndex];
        JSONData.SBATT = parseUtils.getBatteryVolts(payLoadId);
        JSONData.logPeriodMinutes = parseUtils.getLastXBit(JSONData.loggerSpeed, 7) * 15;
        JSONData.STIME = moment().format('DD/MM/YYYY HH:mm:ss');
        JSONData.logPeriodMinutesUSSamplingSpeed = parseUtils.getByteBit((JSONData.loggerSpeed >>> 0).toString(2), 7) ? 15 : 1;
        JSONData.CREASON = parseUtils.getLastXBit(payLoadId[3], 6);
        let heightUByte = payLoadId[(i + 2) + messageStartIndex] & 3;
        JSONData.Aux = payLoadId[i + messageStartIndex] >> 4;
        JSONData.ITEMP = ((payLoadId[(i + 1) + messageStartIndex] >> 1) - 30) * 9/5 + 32;
        const srssi = payLoadId[i + messageStartIndex] & 15;
        JSONData.SRSSI = srssi
        const SRC = (payLoadId[(i + 2) + messageStartIndex] & 60) >> 2;
        JSONData.SRC = SRC
        /****
        checking the ull here is just for the initial call to get an approxamation of the level pre config.
        ****/
        let ull =  (heightUByte << 8) + payLoadId[(i + 3) + messageStartIndex]
        if(Number(ull) > 100 ){
          ull = ull.toString().slice(0, 2)
        }
        JSONData.ULL = ull;
        JSONData.BUND = Boolean(payLoadId[i + 1] & 1)
        if(srssi == 10 && SRC == 10){
        PayloadMessage.push(JSON.stringify(data)) 
      }
      
    }

    
    const thenData = {
      payLoad: PayloadMessage,
      active:monitorActive,
      state:stateMessage
    }
    resolve(thenData);
  })
  .then((data)=>{

      socket.attachment = {
        broadcast: {
          payload: RAW_DATA.toString(),
          parsed: data.payLoad.map(s => JSON.parse(s))
        },
        state: data.state
      };

    if(!socketHashMap[data.state.serial]){
        socketHashMap[data.state.serial] = {
          socket:socket,
          dataCount: 0
        }
    }
   
    socketHashMap[data.state.serial].dataCount++; 
    //endConnection takes the whether the monitor is active, a state obj and the socket! ALL ARE NEEDED!!!!

    console.log(data.payLoad.length, 'length')
    console.log((data.payLoad[0]), 'outgoing')

    if(!data.payLoad.length) {
      console.log("PAYLOAD DOES NOT EXIST, NOT SENDING TO SERVER");

      endConnection(data.active, data.state, socketHashMap[data.state.serial], null, {
        broadcast: {
          payload: RAW_DATA.toString(),
          parsed: data.payLoad
        },
        state: data.state
      })

      return Promise.resolve(true)

    } else {
      url = "http://8.39.160.53:343"
      
      console.log(`POSTING PAYLOAD THEN ENDING CONNECTION! ${data.state.serial} TO URL ${url}`);
      
      return axios.post(url, JSON.parse(data.payLoad[0]))

      .then(res => {

          const response = res.data;

          console.log(`POSTED PAYLOAD FOR ${data.state.serial} SUCCESSFULLY | RESPONSE ${JSON.stringify(response)}`);

          let timeSetting = null;

          if ( response.hasOwnProperty("SSTART") 
            && response.hasOwnProperty("SOFF")) {
              timeSetting = {
                hour: response.SSTART,
                min: response.SOFF
              }
          }

          endConnection(
            data.active, 
            data.state, 
            socketHashMap[data.state.serial], 
            timeSetting,
            {
              broadcast: {
                payload: data.toString(),
                parsed: RAW_DATA.payLoad
              },
              state: data.state
            }
          )

      }).catch( err => {

        alert({

          "SERIAL": data.state.serial,
          "ERROR": err.message || err,
          "PAYLOAD": JSON.parse(data.payLoad[0]),
          "URL": url,
          "TRACE": {
              broadcast: {
                payload: RAW_DATA.toString(),
                parsed: data.payLoad
              },
              state: data.state
            },
        }, "FRONTDOOR SUBMISSION OF LEVEL")

        console.log(`THERE WAS AN ERROR POSTING ${data.state.serial} TO URL ${url}! ENDING CONN`);
        console.log(err);

        console.log(`THERE WAS AN ERROR POSTING ${data.state.serial} TO URL ${url}! ENDING CONN`);
        console.log(err);        
      
        endConnection(data.active, data.state, socketHashMap[data.state.serial], {
            broadcast: {
              payload: data.toString(),
              parsed: data.payLoad
            },
            state: data.state
          })

      })
    }


    })
}


module.exports = TEK643

//0603e2c1890a7e0861075026529859047b000f8a0000ffe004000a5c28fa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000684
//TEK643('0603e208880d7e0861075026498055087b00048003f0ffe003140a0029e30a0029e50a0029e50a0029e60a0029e60a0029e40a0029e40a0029e50a0029e50a0029e5000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b094')
