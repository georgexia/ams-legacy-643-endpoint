const axios = require('axios');
const moment = require('moment');
const url = process.env.TMSURL || 'https://dev.angusdev.com/tms';

//strait forward handler for the 608p, adding the type and name aswell as reformatting to look like all other monitors.


const TEK608 = (data)=>{
  console.log(data, 'incoming');

  data.JSONData.NAME = 'TEK608P - Propane';
  data.JSONData.KEY = 3;


  if (!data.JSONData.hasOwnProperty("ULL")) {

    console.log("TEK608 | MODEM HANDSHAKE");
    console.log("TEK608 | RESPONDING WITH RTC AND IGNORING PAYLOAD DROP");

    return Promise.resolve({
      "d": {
        "RTC": moment().format('D/M/YYYY HH:mm:ss')
      }
    })
  }

  console.log('TEK608 | REPLACING SERIAL WITH RFADDR');

  data.JSONData.SERIAL = data.JSONData.RFADR;

  console.log('TEK608 | CURRENT PAYLOAD');
  console.log(data);

	console.log('TEK608 | set643settings');

  axios.post(
      `https://support.paygo.net/tms/set643settings`, 
      {
      	serial: data.JSONData.SERIAL,
      	isActive: true,
        rssi: 100
      } 
    ).then(res => console.log(`TEK608 | set643settings FOR ${data.JSONData.SERIAL}`, res.data))
     .catch(err => console.log(`TEK608 | set643settings ERROR FOR ${data.JSONData.SERIAL}`, err))
	 
  return axios.post(
  	 "http://8.39.160.53:343", 
  	data
  ).then(res => {
    return {
      "d": res.data
    }
  })
  
}

module.exports = TEK608;
