 const messageToHex = (str)=> {
	 return str.split('').map((s, i)=>{
			return str.charCodeAt(i).toString(16)
	 }).join('')

}
const hexToMessage = (str)=>{
	return str.match(/.{1,2}/g).map(function(v){
      return String.fromCharCode(parseInt(v, 16));
    }).join('');
}
// console.log(messageToHex('TEK643,S0=E0,S1=01,S2=012816,S3=84,S4=412C,S5=C2ee,S6=4096,S7=00,S8=00,S9=,S11=TEK643,S12=c2.korem2m.com,S13=,S14=,S15=monitors.paygo.net,S16=343,S17=0E10,S20=00,S21=C8,S23=13,S24=00,S26=6'))
module.exports = {
	messageToHex:messageToHex,
	hexToMessage:hexToMessage
}