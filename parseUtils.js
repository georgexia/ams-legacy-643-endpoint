// //convert BCD encoded IMEI number to string
// _imei = string.Empty;
// for (int i = 0; i < 8; i++)
// {
//     byte imeiDigits = (packetHeader[7 + i]);    // each byte contains 2 digits of the IMEI
//     _imei += imeiDigits.ToString("X2");         // converting to hex gets our ASCII digits
// }
//
// // strip out the leading zero from the IMEI if there is one
// if (_imei.Substring(0, 1) == "0")
// {
//     _imei = _imei.Substring(1);
// }

module.exports = {
   getMonitorId: function (payLoad){

    let string = payLoad.toString('hex');
    let imei = string.toString().slice(15, 30);
      return imei
  },


  //   let imei = payLoad.toString('hex')
  //   return imei.slice(7, 15)
  //   .split('')
  //   .map(item=>{
  //    id += parseInt(item, 16);
  //    console.log(id.length)
  //     return id



  //   }).join('')

  //  },

  getMessages: function (payLoad){
    let messages = [];
    let mes = '';
    payLoad
    .split('')
    .forEach((message, i, load)=>{
      if(i < 9 ){
        load.pop(message)
      }if(i % 5 !== 0){
          mes += message;
      }if(i % 5 === 0){
        messages.push(mes);
        mes = '';
      }
    })
    return messages
  },
  getBatteryVolts: function(payLoad){
    const mask = 31;
    const batteryHash = payLoad[6] & mask;
    return (batteryHash + 30) / 10;
  },
  getLastXBit: function(value, bitsFromRight){
    const mask = (Math.pow(2, bitsFromRight)-1)
    return value & mask
  },
  getByteBit: function(value, position){
    return (value & (1 << position)) > 0
  },
  setBit:function(value, bitPos, bitValue){
    return value |= (bitValue << bitPos);
  }
};
