const net = require('net');
const axios = require('axios');
const http = require('http');
const shortId = require('shortid')
const Event = require('events');
const moment = require('moment');
const httpProxy = require('http-proxy');
const TEK643 = require('./643.handler');
const TEK750 = require('./750.handler');
const TEK608 = require('./608.handler');
const handshake608 = require('./handShake608')
const messageToHex = require('./messageToHex');
const parser = require('http-string-parser');

/*
  talkBack needs to be used when talking to any server that is expecting normal http protocals
  I'm pretty sure there is a socket.writeHead() so revisit may be Good.
 */
const talkBack = `HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nConnection: keep-alive\r\n\r\n`

const askForConfig = 'TEK643,R1=02'

//Socket to handle all the packets.


//use socket.end() when sending anything that does not expect a responce. that way the socket is properly closed.
net.createServer(socket => {
  socket.id = shortId.generate()
  socket.on('connection', conn => {
    console.log(conn.remoteAddress, 'address');
  })
  socket.on('data', data => {
    console.log(data, 'Incoming');
    console.log(socket.id, 'Socket Open');
    let payLoad = data.toString();
    let checker = payLoad.slice(0, 4);
    //the server should not be getting any get requests at this point. need a space after GET 
    if (checker === 'GET ') {
      return socket.end(`${talkBack}${JSON.stringify({err:'Unauthorised'})}`);
    }
    if (checker === 'POST') {
      let httpRequest = parser.parseRequest(payLoad);
      let body = null;
      if (payLoad.includes("AngusDevelopmentTeamTest")) {
      	body = JSON.parse(payLoad);
      } else {
body = JSON.parse(httpRequest.body);
      }

      //this handles the communication for the 750 Gremlin
      if (body['JSONData'] && (!body['JSONData']["RFADR1"] && !body['JSONData']["RFADR"])) {
        console.log(`incoming data Time: ${moment().format('MM-DD-YYYY HH:mm:ss')}    `, payLoad);

        TEK750(body)
          .then(res => {
            const close = httpRequest.headers.Connection;
            const rex = res.data;
            // rex.d = res.data;
            console.log(rex, 'response ', moment().format('MM-DD-YYYY HH:mm:ss'));
            return close === 'close' ? socket.end(`${talkBack}${JSON.stringify(rex)}`) : socket.write(`${talkBack}${JSON.stringify(rex)}`);
          })
          .catch(err => {
            console.log(err);
            console.error(err);
          })
        //this handles the communication for the 608p monitors
        //this handles the talkBack the same way at the moment, until we have more info on the return.
      } else if (body['new']) {
        return handshake608(body)
          .then(res => {
            return socket.end(`${talkBack}${JSON.stringify(res.data)}`);
          })
          .catch(err => socket.end(`${talkBack}${JSON.stringify(err)}`));
      } else {
        return TEK608(body)
          .then(res => {
            
            console.log("TEK608 REPLY:");
            console.log(res);

            return socket.end(`${talkBack}${JSON.stringify(res)}`);
          })
          .catch(err => {
            console.log(err);
            console.error(err);
          })
      }
    } else {
      return TEK643(data, socket)
        .catch(err => {
          console.log("ERR.SERVER")
          console.log(err);
          console.error(err);
        })
    }
  })
  socket.on('error', err => {
    if (socket.attachment) {
      console.log("POSTING ERROR", socket.attachment)


      // axios.post("http://172.16.150.42:6070/event", {
      //   key: "MONITOR",
      //   tag: "643.ENDPOINT",
      //   origin: "SOCKET.ERROR",
      //   detail: JSON.stringify({
      //     "SERIAL": socket.attachment.state.serial,
      //     "ERROR": err.message || err,
      //     "TRACE":socket.attachment,
      //   }, null, 1),
      //   level: "ERROR"
      // }).catch(err => console.error(require("util").inspect(err.response || err)))
    }

    console.log(err, '*FAIL');
    console.error(err, '*FAIL');
  })
  socket.on('close', () => {
    console.log(socket.id, ' Socket closed');
  })
}).listen(8080, () => {
  console.log('connected port 8080');
})
