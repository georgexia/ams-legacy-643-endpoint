const axios = require('axios');
const moment = require('moment');





const handShake608 = (body)=>{
  const serial = body.serial
  const last4 = serial.slice((serial.length - 4), serial.length)
  const monitorURL = `HTTP://TEK608-${last4}`;
  const postData = {'d':{'RTC':moment().format('D/MM/YY HH:mm'), 'FWFN':'./608firm/testFirm608.hex'}}
  return axios.post(monitorURL, JSON.stringify(postData))

}


module.exports = handShake608;
